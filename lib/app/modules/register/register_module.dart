import 'package:flutter_modular/flutter_modular.dart';
import 'package:museum_mobile/app/modules/register/pages/register_bloc.dart';
import 'package:museum_mobile/app/modules/register/pages/register_page.dart';

class RegisterModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => RegisterBloc()),
      ];

  @override
  List<Router> get routers => [
        Router('/', child: (_, args) => RegisterPage()),
      ];

  static Inject get to => Inject<RegisterModule>.of();
}
