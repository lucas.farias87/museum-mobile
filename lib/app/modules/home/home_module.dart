import 'package:flutter_modular/flutter_modular.dart';
import 'package:museum_mobile/app/modules/home/pages/home_bloc.dart';
import 'package:museum_mobile/app/modules/home/pages/home_page.dart';

class HomeModule extends ChildModule {
  @override
  List<Bind> get binds => [
        Bind((i) => HomeBloc()),
      ];

  @override
  List<Router> get routers => [
        Router('/home', child: (_, args) => HomePage()),
      ];

  static Inject get to => Inject<HomeModule>.of();
}
