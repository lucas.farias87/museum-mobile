import 'package:flutter_modular/flutter_modular.dart';
import 'package:museum_mobile/app/app_module.dart';
import 'package:museum_mobile/app/modules/register/pages/register_bloc.dart';
import 'package:museum_mobile/app/modules/register/register_module.dart';

void main() {
  Modular.init(AppModule());
  Modular.bindModule(RegisterModule());
  RegisterBloc bloc;

  // setUp(() {
  //     bloc = RegisterModule.to.get<RegisterBloc>();
  // });

  // group('RegisterBloc Test', () {
  //   test("First Test", () {
  //     expect(bloc, isInstanceOf<RegisterBloc>());
  //   });
  // });
}
